use std::io::{BufReader, BufRead};
use std::fs::File;
use std::env;
use std::process::exit;
use std::collections::HashSet;

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();
    if args.len() < 2 {
        println!("You must specify a file of chords to process.\nrustscales <filename>");
        exit(1);
    }

    let filename = args[1].to_string();

    // Note names in chromatic order
    let notes = ["C", "C#", "D", "D#", "E", "F", "F#", "G", "G#", "A", "A#", "B"];

    // Circle of fifths order indexes into notes only
    let fifths = [0, 7, 2, 9, 4, 11, 6, 1, 8, 3, 10, 5];

    // Read in file of numbers
    let f = File::open(filename)?;
    let mut reader = BufReader::new(f);

    // decode each number if in range
    let mut buf = String::new();
    let mut bytes = reader.read_line(&mut buf)?;
    buf = buf.trim().to_string();
    while bytes > 0 {
        let result = buf.parse::<i32>();

        match result {
            Ok(number) => {
                // Offset is only left 4 bits
                let offset = (number >> 12) & 0x0F;
                let mut flags = HashSet::new();
                for index in 0..12 {
                    let testindex = (index + offset) % 12;
                    let testbit = 1 << (11 - testindex);
                    if number & testbit == testbit {
                        flags.insert(fifths[testindex as usize]);
                    }
                }

                // Now output
                let mut this_chord = String::from("");
                for index in 0..12 {
                    let testindex = (index + offset) % 12;
                    if flags.contains(&testindex) {
                        let note = notes[testindex as usize];
                        this_chord.push_str(note);
                        this_chord.push_str("-");
                    }
                }
                // Remove the last -
                this_chord.pop();
                println!("{this_chord}");
            },
            Err(message) => { println!("{message}"); exit(1) }
        }

        buf.clear();
        bytes = reader.read_line(&mut buf)?;
        buf = buf.trim().to_string();
    }
    Ok(())
}
