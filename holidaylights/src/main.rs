use std::time::Duration;
use std::{env, thread};
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::process::exit;

fn clear_tty_lines(lines: usize)
{
    println!("\x1b[{}J\x1b[{}A", lines, lines);
}

fn main() -> std::io::Result<()> {
    let args: Vec<_> = env::args().collect();
    if args.len() < 3 {
        println!("You must specify a file of colors and a file of artwork.\nholidaylights <colorsfilename> <artfilename>");
        exit(1);
    }

    let colors_filename = args[1].to_string();
    let art_filename = args[2].to_string();

    let default_color = 7;
    let star_color = 3;
    let star_off_color = 0;
    let colors = ["black", // 0
                    "red", // 1
                    "green", // 2
                    "yellow", // 3
                    "blue", // 4
                    "magenta", // 5
                    "cyan", // 6
                    "white", // 7
                ];
                
    // Read in the colors to get our table of mappings

    // list of color indexes to use from colors file
    let mut colors_list: Vec<usize> = vec![];

    let color_file = File::open(colors_filename)?;
    let mut color_reader = BufReader::new(color_file);
    let mut buf = String::new();
    let mut bytes = color_reader.read_line(&mut buf)?;
    buf = buf.trim().to_string();
    while bytes > 0 {
        if colors.contains(&buf.as_str()) {
            let result = colors.iter().position(|&s| s == buf);
            match result {
                Some(index) => { colors_list.push(index); },
                None => { println!("Unable to find index of {buf} in colors list"); }
            }
        }
        buf.clear();
        bytes = color_reader.read_line(&mut buf)?;
        buf = buf.trim().to_string();
    }

    let clear_color = "\x1b[0m";

    let art_file = File::open(art_filename)?;
    let mut art_reader = BufReader::new(art_file);
    buf.clear();
    bytes = art_reader.read_line(&mut buf)?;

    let mut art_lines: Vec<String> = vec![];
    while bytes > 0 {
        // Convert any letter characters to their corresponding ornament with color
        buf = buf.trim_end().to_string();
        art_lines.push(buf.to_string());

        buf.clear();
        bytes = art_reader.read_line(&mut buf)?;
    }

    let mut output = String::new();
    let mut offset: usize = 0;
    let numcolors = colors_list.len();
    loop {
        for line in art_lines.iter() {
            // Convert any letter characters to their corresponding ornament with color
            output.clear();

            for c in line.chars() {
                if ('a'..'z').contains(&c) {
                    // replace it with the bulb (or z later for star)
                    // Find the color index
                    let color_index = (c as u32 - 'a' as u32) as usize;
                    let mut color = default_color;
                    if color_index < numcolors {
                        color = colors_list[(color_index + offset) % numcolors];
                    }
                    output.push_str(format!("\x1b[3{}m", color).as_str());
                    output.push('●');
                    output.push_str(clear_color);
                } else if 'z' == c {
                    // It's the star, so use a star character and make it blink
                    let mut color = star_color;
                    if offset % 2 == 1 {
                        color = star_off_color;
                    }
                    output.push_str(format!("\x1b[3{}m", color).as_str());
                    output.push('★');
                    output.push_str(clear_color);
                } else {
                    // not a special character, so just copy it directly
                    output.push(c);
                }
    
            }
            // print out the converted line
            println!("{}", output);
        }

        thread::sleep(Duration::from_millis(500));
        clear_tty_lines(art_lines.len() + 1);
        offset += 1;
        offset %= numcolors;
    }

    Ok(())
}
